/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

public class Runinfo {

    private String runinfo_id; //评测编号
    private String process_id;//每次运行流水号
    private String datacfg_id;//数据规模编号
    private String runinfo_strtm; //开始时间
    private String runinfo_endtm; //结束时间
    private String runinfo_costtime; //耗时
    private String runinfo_cfg; //运行配置

    public String getRuninfo_id() {
        return runinfo_id;
    }

    public void setRuninfo_id(String runinfo_id) {
        this.runinfo_id = runinfo_id;
    }

    public String getRuninfo_strtm() {
        return runinfo_strtm;
    }

    public void setRuninfo_strtm(String runinfo_strtm) {
        this.runinfo_strtm = runinfo_strtm;
    }

    public String getRuninfo_endtm() {
        return runinfo_endtm;
    }

    public void setRuninfo_endtm(String runinfo_endtm) {
        this.runinfo_endtm = runinfo_endtm;
    }

    public String getRuninfo_costtime() {
        return runinfo_costtime;
    }

    public void setRuninfo_costtime(String runinfo_costtime) {
        this.runinfo_costtime = runinfo_costtime;
    }

    public String getRuninfo_cfg() {
        return runinfo_cfg;
    }

    public void setRuninfo_cfg(String runinfo_cfg) {
        this.runinfo_cfg = runinfo_cfg;
    }

	public String getProcess_id() {
		return process_id;
	}

	public void setProcess_id(String process_id) {
		this.process_id = process_id;
	}

	public String getDatacfg_id() {
		return datacfg_id;
	}

	public void setDatacfg_id(String datacfg_id) {
		this.datacfg_id = datacfg_id;
	}
	public Runinfo() {
		
	}
	public Runinfo(String runinfo_id, String process_id, String datacfg_id, String runinfo_strtm, String runinfo_endtm,
			String runinfo_costtime, String runinfo_cfg) {
		super();
		this.runinfo_id = runinfo_id;
		this.process_id = process_id;
		this.datacfg_id = datacfg_id;
		this.runinfo_strtm = runinfo_strtm;
		this.runinfo_endtm = runinfo_endtm;
		this.runinfo_costtime = runinfo_costtime;
		this.runinfo_cfg = runinfo_cfg;
	}
    
    
}
