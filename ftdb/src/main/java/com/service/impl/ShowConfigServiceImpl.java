/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.common.constant.TrancfgConstant;
import com.mapper.ReadDataConfig;
import com.mapper.ReadTranConfig;
import com.pojo.DataConfig;
import com.pojo.Trancfg;
import com.service.ShowConfigService;

@Service
@Scope("prototype")
public class ShowConfigServiceImpl implements ShowConfigService{
	@Autowired
	private ReadDataConfig readDataConfig;
	
	@Autowired
	private ReadTranConfig readTranConfig;
	
	@Override
	public String getDataConfig() {
		List<DataConfig> data = new ArrayList<>();
		List<Map<String,String>> ret = new ArrayList<>();
		data =  readDataConfig.readAllDataCfg();
		for (int i = 0; i < data.size(); i++) {
			Map<String,String>  m = new HashMap<>();
			m.put(data.get(i).getDatacfg_id(), getStr(data.get(i)));
			ret.add(m);
		}
		String json = JSON.toJSONString(ret);
		return json;
	}

	private String getStr(DataConfig data) {
		StringBuffer sb = new StringBuffer();
		sb.append("网点数：").append(data.getDatacfg_brhnum()).append("; ");
		sb.append("科目数：").append(data.getDatacfg_sjnonum()).append("; ");
		sb.append("账号数：").append(data.getDatacfg_accnum()).append("; ");
		sb.append("客户数：").append(data.getDatacfg_cusnum());
		return sb.toString();
	}

	@Override
	public String getTranConfig() {
		Map<String,List<Trancfg>> ret = new HashMap<String,List<Trancfg>>();
		Trancfg param = new Trancfg();
		List<Trancfg>  data =  readTranConfig.readTranConfig(param);
		for (Trancfg trancfg :data) {
			if(TrancfgConstant.TRANCFG_ACCOUNTS_INVENTORY.equals(trancfg.getTrancfg_id())) {
				continue;
			}
			if(ret.containsKey(trancfg.getTrancfg_testid())) {
				ret.get(trancfg.getTrancfg_testid()).add(trancfg);
			}else {
				List<Trancfg> temp = new ArrayList<Trancfg>();
				temp.add(trancfg);
				ret.put(trancfg.getTrancfg_testid(), temp);
			}
		}
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		for (String key : ret.keySet()) {
			List<Trancfg> list = ret.get(key);
			String id = "";
			String value = "";
			for(Trancfg trancfg:list) {
				id = trancfg.getTrancfg_testid();
				value += trancfg.getTrancfg_name()+trancfg.getTrancfg_procnum()+"线程";
				value += "运行"+trancfg.getTrancfg_runnum()+";";
			}
			Map<String,String> valueMap = new HashMap<>();
			valueMap.put(id, value);
			result.add(valueMap);
		}
		String json = JSON.toJSONString(result);
		return json;
	}


}
