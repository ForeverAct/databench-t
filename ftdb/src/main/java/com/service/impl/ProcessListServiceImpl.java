/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.localMapper.ClustercfgMapper;
import com.localMapper.ProcessListMapper;
import com.mapper.RuninfoMapper;
import com.pojo.Clustercfg;
import com.pojo.ProcessList;
import com.pojo.ProcessListVo;
import com.pojo.Runinfo;
import com.service.ProcessListService;

@Service
@Scope("prototype")
public class ProcessListServiceImpl implements ProcessListService {
	@Autowired
	private ClustercfgMapper clustercfgMapper;
	@Autowired
    private ProcessListMapper processListMapper;
    @Autowired
    private RuninfoMapper runinfoMapper;
	public ProcessList getProcessStatus(String uuid) {
		ProcessList status = processListMapper.getProcessStatus(uuid);
		return status;
	}

	@Override
	public List<ProcessListVo> getProcessList() {
		List<ProcessList> list = processListMapper.getProcessList();
		List<ProcessListVo> listVo = new ArrayList<ProcessListVo>();
		if(list!=null&&list.size()>0) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");   
			for (ProcessList processList : list) {
				Runinfo runinfo = runinfoMapper.getRuninfoConfig(processList.getProcess_id());
				ProcessListVo processListVo = new ProcessListVo();
				processListVo.setProcess_id(processList.getProcess_id());
				processListVo.setProcess_content(processList.getProcess_content());
				processListVo.setProcess_time(df.format(processList.getProcess_time()));
				if(runinfo!=null) {
					processListVo.setDatacfg_content(runinfo.getRuninfo_cfg());
				}else {
					processListVo.setDatacfg_content("");
				}
				listVo.add(processListVo);
			}
		}
		return listVo;
	}

	@Override
	public List<Clustercfg> getClusterList() {
		return clustercfgMapper.getClustercfg();
	}

}
